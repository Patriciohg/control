#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include "estudiantes.h"

int contador_alumnos(estudiante curso[]){// funcion creada para saber el numero de alumnos en la lista
	int i = 0,cont = 0;
	while(strcmp(curso[i].nombre,"\0") != 0){
		i++;
		cont++;
	}
	return cont;
}
float desvStd(estudiante curso[]){
	float promedio_general = 0, base_total, diferencia, diferencia_cuadrado, desviacion_tipica = 0;// se declaran las variables que usaremos para sacar la desviacion estandar
	int i = 0, cont = 0, cont_sobre_media = 0, suma;// se declaran contadores y la "sumatoria"
	while(curso[i].prom != 0.0){// se crea un ciclo while para sacar el promedio general y el numero de almunos
		promedio_general += curso[i].prom;//se suman todos los promedios
		i++;
		cont++;// este contador representa el numero de almunos
	}
	if(cont == 0){
		printf("EROR AUN NO SE INGRESAN NOTAS AL SISTEMA\n");
		return 0;
	}
	promedio_general = promedio_general/cont;//se saca el promedio de todos lo promedios sumados
	printf("numero de almunos : %d\n",cont); 
	printf("el promedio general o media del curso fue : %f\n",promedio_general);

	i = 0;
	while (i<cont){// ciclo while para contar el nunero de alumnos con promedio sobre la media
		if (curso[i].prom > promedio_general){// se revisan los alumnos con promedio mayor a la media
			cont_sobre_media++;// se le suma al contador de estos
		}
		diferencia = curso[i].prom -promedio_general;//se toma la diferencia de los promedios restados por el promedio general
		diferencia_cuadrado = diferencia*diferencia;// se saca el cuadrado de esto
		suma += diferencia_cuadrado;// se añade a la "sumatoria de estos"
		i++;
	}
	base_total = suma/cont;//se saca la base total con el promedio de las sumas totales
	desviacion_tipica= sqrt(base_total);//se obtiene la desviacion con la funcion de razi cuadrada de math.h
	printf("alumnos sobre la media : %d\n",cont_sobre_media);
	return desviacion_tipica;// se retorna las desviacion estandar
}

float menor(estudiante curso[]){//funcion para sacar el promedio menor
	float menor = 7.0;
	int i = 0;
	int numero_alumnos = contador_alumnos(curso);// se toma el numero de almunos
	for(i= 0;i<numero_alumnos;i++){//se itera desde 0 hasta el numero de alumnos-1
		if(menor > curso[i].prom && curso[i].prom != 0 ){// si el menor es mayor que el promedio de el almuno en la posisicon i
			menor = curso[i].prom;// este pasa hacer el menor
		}
	}
	return menor;// retornamos el menor obtenido de todas las iteraciones
}
float mayor(estudiante curso[]){
	float mayor = 1.0;// se realiza el mismo procedimiento que hicimos para sacar el menor pero en este caso pero con el menor
	int  i = 0;
	while(curso[i].prom != 0){
		if(mayor<curso[i].prom){
			mayor = curso[i].prom;
		}
		i++;
	}
	return mayor;
}
void registroCurso(estudiante curso[]){
	float porc_proy= 0.0, porc_control= 0.0, prom_proyectos = 0.0, prom_controles = 0.0;
	int numero_alumnos = contador_alumnos(curso);// se utiliza una funcion para saber el numero de  almunos en la lista

	printf("Ingrese el pocertaje total de los proyectos\nej: si es 70 porciento debe ingresar 0.70\n");
	scanf("%f",&porc_proy);
	printf("Ingrese el porcentaje total de los controles\nej: si es 30 porciento debe ingresar 0.30 : \n");
	scanf("%f",&porc_control);
	if (porc_control + porc_proy != 1){// se comprueba que los porcentajes ingresados sean distinto al 100%
		printf("error has ingresado mal lo porcentajes\n");
		printf("Ingrese el pocertaje total de los proyectos \nej: si es 70 porciento debe ingresar 0.70\n");
		scanf("%f",&porc_proy);
		printf("Ingrese el porcentaje total de los controles \nej: si es 30 porciento debe ingresar 0.30 : \n");
		scanf("%f",&porc_control);
	}
	printf("El numero de almunos es %d: \n",numero_alumnos);
	for (int i =0 ; i<numero_alumnos; i++){// se inicia un ciclo for para que se ingresen las notas
		printf("a continuacion se iran mostrando los distintos nombre de los alumnos y usted debera ingresar la nota\n"); 
		printf("%s %s %s\n",curso[i].nombre ,curso[i].apellidoP, curso[i].apellidoM);
		printf("nota proyecto 1 : \n");
			scanf("%f",&curso[i].asig_1.proy1);
		printf("nota proyecto 2 : \n");
			scanf("%f",&curso[i].asig_1.proy2);
		printf("nota proyecto 3 : \n");
			scanf("%f",&curso[i].asig_1.proy3);
		printf("nota control 1 : \n");
			scanf("%f",&curso[i].asig_1.cont1);
		printf("nota control 2 : \n");
			scanf("%f",&curso[i].asig_1.cont2);
		printf("nota control 3 : \n");
			scanf("%f",&curso[i].asig_1.cont3);
		printf("nota control 4 : \n");
			scanf("%f",&curso[i].asig_1.cont4);
		printf("nota control 5 : \n");
			scanf("%f",&curso[i].asig_1.cont5);
		printf("nota control 6 : \n");
			scanf("%f",&curso[i].asig_1.cont6);
		prom_proyectos = (curso[i].asig_1.proy1 + curso[i].asig_1.proy2+curso[i].asig_1.proy3)/3;// se saca el promedio de las notas de lo proyectos
		prom_controles = (curso[i].asig_1.cont1 +curso[i].asig_1.cont2 + curso[i].asig_1.cont3+ curso[i].asig_1.cont4+ curso[i].asig_1.cont5+ curso[i].asig_1.cont6)/6;// se saca el proedio de los controles
		curso[i].prom=prom_proyectos * porc_proy + prom_controles*porc_control; // se ingresa el promedio en el arreglo estructurado curso
		printf(" EL promedio del alumno :\n %s %s Fue: %2f \n",curso[i].nombre ,curso[i].apellidoP, curso[i].prom);
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	FILE *aprobados;// se definen punteros para hacer el manejo de archivos
	FILE *reprobados;
	int i = 0;
	char lista_aprobado[1024]="";// se define un arreglo de "cadena de caracteres"
	char lista_reprobados[1024]="";
	aprobados=fopen("aprobados.txt", "w");// se abren los archivos
	reprobados=fopen("reprobados.txt", "w");
	if(aprobados==NULL || reprobados==NULL){// se comprueba que los archivos pudieron ser creados
		printf("error el archivo no puede ser creado\n");
	}
	while(curso[i].prom != 0.0){//se crea un ciclo while para ir llenando el archivo 
		if(curso[i].prom >= 4.0 && curso[i].prom != 0){// se van viendo los aprobados
			strcat(lista_aprobado ,curso[i].nombre);
			strcat(lista_aprobado, "	");//se van añadiendo espacios para separar los nombres y apellidos
			strcat(lista_aprobado ,curso[i].apellidoP);
			strcat(lista_aprobado, "	");
			strcat(lista_aprobado, curso[i].apellidoM);
			strcat(lista_aprobado, "	");
			printf("aprobado >>>> %s %2f\n",lista_aprobado, curso[i].prom);//se muestra lo que se insertara 
			fprintf(aprobados,"%s %2f\n",lista_aprobado,curso[i].prom);// se añaden los datos al archivo
			strcpy(lista_reprobados,"");//se vacia el arreglo
		}else{// se va llenando el arreglo de reprobados
			strcat(lista_reprobados ,curso[i].nombre);
			strcat(lista_reprobados, "	");// se añaden espacios para separar nombre apellido y demas
			strcat(lista_reprobados ,curso[i].apellidoP);
			strcat(lista_reprobados, "	");
			strcat(lista_reprobados, curso[i].apellidoM);
			strcat(lista_reprobados, "	");
			printf("reprobado >>>> %s %2f\n",lista_reprobados,curso[i].prom);
			fprintf(reprobados, "%s %2f\n",lista_reprobados,curso[i].prom);//se ingresan los datos al archivo
			strcpy(lista_aprobado,"");
		}
		i++;
		}
	fclose(reprobados);//se cierran los archivos
	fclose(aprobados);
}
void metricasEstudiantes(estudiante curso[]){ 
	float desviacion = desvStd(curso);//se obitene la desviacion estandar para luego mostrar en pantalla
	float promedio_menor = menor(curso);
	printf("\n La desviacion tipica es : %2f\n El mejor promedio fue : %2f\n el promedio menor fue : %2f\n", desviacion, mayor(curso), promedio_menor);
}

void menu(estudiante curso[]){	
int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}